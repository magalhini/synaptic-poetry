const natural = require('natural')
const { promisify } = require('util')
const fs = require('fs')

const encoding = { encoding: 'utf8' }
const wordTokenizer = new natural.WordTokenizer()
const readFileAsync = promisify(fs.readFile);

async function readFile(path) {
  try {
    return await readFileAsync(path, encoding);
  } catch(e) {
    console.log('Err:', e)
  }
}

function sentencify(str) {
  return Promise.resolve(str.split('\n').filter(n => n.length > 10).map(n => n.trim()))
}

function normalise(data, output) {
  const normalised = data.map(sentence => ({
      input: sentence,
      output: output
    })
  )

  return Promise.resolve(normalised)
}

// const t = wordTokenizer.tokenize()

readFile('./data/romeo.txt')
  .then(text => sentencify(text))
  .then(list => normalise(list, 'Shakespeare'))
  .then(s => console.log(s))
