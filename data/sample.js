
const trainingData = [
    {
        input: "So true, thank you!",
        output: [0,1]
    },{
        input: "Inside Chi's nursery",
        output: [0,1]
    },{
        input: "Why I dyed my hair pink",
        output: [0,1]
    },{
        input: "Feeling Blue (wearing @kkwbeauty powder contour in medium & dark contour kit as eye shadow, & a new lip coming soon)",
        output: [0,1]
    },{
        input: "I love my mom so much as a blonde!! @KrisJenner @KUWTK",
        output: [0,1]
    },{
        input: "I don’t really do wigs. It’s real.",
        output: [0,1]
    },{
        input: "Wait the thought of going back dark makes me sad. @MyleezaKardash may have influenced me",
        output: [0,1]
    },{
        input: "I went live on instagram! Thank you my BFF Allison for the fun interview and hank you to Creat & Cultivate for having me!",
        output: [0,1]
    },{
        input: "On my way the the @createcultivate event! I’m being interviewed about business by my bff Allison. This should be fun!",
        output: [0,1]
    },{
        input: "The sweetest! Best baby! She looks a tiny bit like North and a tiny bit like Saint but definitely her own person!",
        output: [0,1]
    },{
        input: "Fashion Week is so much fun in the #KimKardashianGame! I'd love to see what you're wearing! http://smarturl.it/PlayKKH",
        output: [0,1]
    },{
        input: "Congressman Schiff omitted and distorted key facts @FoxNews So, what else is new. He is a total phony!",
        output: [1,0]
    },{
        input: "I will be interviewed by @JudgeJeanine on @FoxNews at 9:00 P.M. Enjoy!",
        output: [1,0]
    },{
        input: "Dem Memo: FBI did not disclose who the clients were - the Clinton Campaign and the DNC. Wow!",
        output: [1,0]
    },{
        input: "The Democrat memo response on government surveillance abuses is a total political and legal BUST. Just confirms all of the terrible things that were done. SO ILLEGAL!",
        output: [1,0]
    },{
        input: "Unemployment claims are at the lowest level since 1973. Much of this has to do with the massive cutting of unnecessary and job killing Regulations!",
        output: [1,0]
    },{
        input: "So true Wayne, and Lowest black unemployment in history!",
        output: [1,0]
    },{
        input: "Thank you to the great men and women of the United States @SecretService for a job well done!",
        output: [1,0]
    },{
        input: "Today, @FLOTUS Melania and I were honored to welcome Prime Minister @TurnbullMalcolm and Mrs. Turnbull of Australia to the @WhiteHouse!",
        output: [1,0]
    },{
        input: "After years of rebuilding OTHER nations, we are finally rebuilding OUR nation - and we are restoring our confidence and our pride!",
        output: [1,0]
    },{
        input: "So true, thank you!",
        output: [1,0]
    }
]

module.exports = trainingData
