const synaptic = require('synaptic')
const utils = require('./utils')
const sampleData = require('./data/sample')

const { Neuron, Layer, Trainer, Architect, Network } = synaptic
const { encode } = utils

function Perceptron (input, hidden, output) {
  const inputLayer = new Layer(input)
  const hiddenLayer = new Layer(hidden)
  const outputLayer = new Layer(output)

  inputLayer.project(hiddenLayer)
	hiddenLayer.project(outputLayer)

  this.set({
		input: inputLayer,
		hidden: [hiddenLayer],
		output: outputLayer
	})
}

Perceptron.prototype = new Network()
Perceptron.prototype.constructor = Perceptron

const trainingData = sampleData.map((item) => ({
    input: utils.encode(item.input),
    output: item.output
  }))

const myPerceptron = new Perceptron(2,4,2)
const myTrainer = new Trainer(myPerceptron)

myTrainer.train(trainingData, {
  rate: .2,
  iterations: 10000,
  shuffle: true
})

const testSentence1 = 'FBI and America are terrible and our pride'
const testSentence2 = 'My hair dying should be fun!'

console.log(myPerceptron.activate(utils.encode(testSentence1)))
console.log(myPerceptron.activate(utils.encode(testSentence2)))
