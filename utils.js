function encode(arg) {
   return arg.split('').map(x => (x.charCodeAt(0) / 255));
}

module.exports = {
  encode
}
